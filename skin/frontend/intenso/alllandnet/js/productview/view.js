jQuery(function(){
		
(function($){
		
		
		$(".sub-nav a").on("click",function(e){
			var val = $(this).attr("data-link");
			//var _scrollTop = $(window).scrollTop();
			switch(val){
			case '#additional-info' :
				$(".product-collateral-wrapper .additional").show().siblings().hide();
				/*$(".product-collateral-wrapper .additional").siblings().hide(1300,function(){
					$(".product-collateral-wrapper .additional").show(1300);
				});*/
				  break;
			case '#ratings-reviews' :
				$(".product-collateral-wrapper .ratings-reviews").show().siblings().hide();
				/*$(".product-collateral-wrapper .ratings-reviews").siblings().hide(1300,function(){
					$(".product-collateral-wrapper .ratings-reviews").show(1300);
				});*/
			  	  break;
			case '#accordion' :
				$(".product-collateral-wrapper > div:last").show().siblings().hide();
				/*$(".product-collateral-wrapper > div:last").siblings().hide(1300,function(){
					$(".product-collateral-wrapper > div:last").show(1300);
				});*/
		      	  break;
		    default:
		    	$(".product-collateral-wrapper .description").show().siblings().hide();
		    	/*$(".product-collateral-wrapper .description").siblings().hide(1300,function(){
		    		$(".product-collateral-wrapper .description").show(1300);
				}) ;*/
			}
			//$(window).scrollTop(_scrollTop);
			/*if($j("#magellan-nav[class*='magellan-fixed']")[0]){
				$(window).scrollTop(813);
			}*/
			$(this).parent().addClass("active").siblings().removeClass("active");
			
		});
		
		
		
		$(".gobuy").on("click",function(){
			var fmp_sku = document.getElementById("fmp_sku").innerHTML;
			var fmp_name = document.getElementById("fmp_name").innerHTML;
			var fmp_price = document.getElementById("fmp_price").innerHTML;
			var fpm_currency = document.getElementById("fpm_currency").innerHTML;
			var uid="";

			!function(w,o,s,r,p,k,e){if(w.mkq){return}p=w.mkq=function()
			{p.invokeFunc?p.invokeFunc.apply(p,arguments):p.qu.push(arguments)};if(!w._mkq){w._mkq=p}p.push=p;p.qu=[];k=o.createElement(s);k.async=!0;k.src=r;e=o.getElementsByTagName(s)[0];e.parentNode.insertBefore(k,e)}(window,document,"script","//pixeltrack.clientgear.com/mkq.min.js");
			uid?mkq('init', '101119263398094',uid): mkq('init', '101119263398094');

			//购物车
			mkq('track','AddToCart',{
				content_name: fmp_name,
				content_ids: fmp_sku,
				content_type: 'product',
				value: fmp_price,
				currency: fpm_currency
			});
			productAddToCartForm.submit(this);
		});
        $(".count .reduce").on("click",function(e){
        	e.preventDefault();
        	var val = parseInt($("#qty").attr("value"));
        	if(val > 1){
                   val--;
        	   $("#qty").attr("value",val);
        	}
        });
        $(".count .add").on("click",function(e){
        	e.preventDefault();
        	var val = parseInt($("#qty").attr("value"));
                   val++;
                   $("#qty").attr("value",val);
        }); 
		
		
        if($(".magellan-nav dl dd").length > 1){//nav
			var num = $(".magellan-nav dl dd").length;
			var width = 1/num * 100 ;
			$(".magellan-nav dl dd").each(function(){
				$(this).css("width",width+"%");
				$(this).css("text-align","center");
			});
		}
		
		
		$(window).scroll(function(){
			
			var scrollHeight = $(window).scrollTop();
			var windowHeight = $(window).height();;
			$("#product-description,#additional-info,#ratings-reviews,#accordion").each(function(){
				var eleOffset = $(this).offset().top;//distance from current element to the top of brower;
				var num = parseInt(eleOffset - scrollHeight);
				if(num > 0 && num < parseInt(windowHeight/2) ){
					$("a[data-link='#"+this.id+"']").parent().addClass("active").siblings().removeClass("active");
				}else if(num > parseInt(windowHeight*4/5) && num < windowHeight){
					if(this.id == "additional-info"){
						$("a[data-link='#product-description']").parent().addClass("active").siblings().removeClass("active");
					}else if(this.id == "ratings-reviews"){
						$("a[data-link='#additional-info']").parent().addClass("active").siblings().removeClass("active");
					}else if(this.id == "accordion"){
						$("a[data-link='#ratings-reviews']").parent().addClass("active").siblings().removeClass("active");
					}
				}
			});
		});
		
		$(".ship-log_b").on("click",function(){
			$(".ship-log_c").addClass("dialogs_show");
			$("#bm_dialogs_bg").show();
		});
		$(".close_dialogs").on("click",function(){
			$(".ship-log_c").removeClass("dialogs_show");
			$("#bm_dialogs_bg").hide();
		});
		$(".selectFlag").on("mouseover",function(){
			$(".pu_blockWarp").removeAttr("style");
		});
		$("#shipFromList li").on("click",function(){
			$(this).addClass("selectActive").siblings().removeClass("selectActive");
			var _class = $(this).attr("data-attr-value");
			$("#current_country_stockin").removeAttr("class").addClass("flag_"+_class);
			$(".stock_in > em").html($(this).text().trim());
		});
		$(".search_country input").on("keyup",function(){
			$("ul[class=country_list] > li span").each(function(){
				$(this).parent().show();
			});
			var val = $(this).val().trim();
			if(val != ""){
				$("ul[class=country_list] > li span").each(function(){
					if(!$(this).html().toLowerCase().match(val)){
						$(this).parent().hide();
					}
				});
			}else{
				return;
			}
		});
		$("ul[class=country_list] > li").on("click",function(){
			var val = $(this).children("span").html();
			var _flagTxt = $(".flag_Txt").html();
			if(val == _flagTxt){
				return;
			}
			var addClass = $(this).children("span").attr("data");
			$("#current_country_flage").removeAttr("class").addClass("flag_"+addClass);
			$(".flag_Txt").html(val);
			$(".pu_blockWarp,.newshopping_address").hide();
			
			$.ajax({
				url:'/skin/frontend/intenso/alllandnet/js/json/getcoupon.json',
				//url:'/business/' + urlSuffix,
				data:"",
				success:function(data){
					 if(data){
						 $(".newshopping_address").show();
					 }
				
				}
			})
		});


})(jQuery)
})